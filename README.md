# README #
### Budowanie projektu ###
Projekt zarządzany jest przez narzędzie Stack - polecam zainstalować.
```
stack setup
stack build --ghc-options "-O2 -threaded -rtsopts"
```

### Uruchamianie testów:###
```
stack test
```
### Uruchamianie solvera
Dobrze jest uruchamiać z opcjami.
```
solver-exe +RTS -N1 -H256m
```