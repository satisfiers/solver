module Main where

import DPLL
import CNF
import Control.Monad
import Control.Applicative
import Text.Printf
import qualified Data.Vector.Unboxed as U

printSolution :: U.Vector Int -> IO ()
printSolution bindings = do
  putStr "v "
  U.mapM_ (putStr . printf "%d ") (U.tail bindings)
  putStrLn ""

main :: IO ()
main = do
    contents <- filter (not . null) . map words . lines <$> getContents
    let input = map init . dropWhile (\line -> head line `elem` ["c", "p"]) $ contents
        integers = map (map read) input :: [[Int]]
        cnf = fromIntsCNF integers

        solution = dpllUP cnf

    case solution of
      Unsat -> putStrLn "s UNSATISFIABLE"
      Sat bindings -> do
        putStrLn "S SATISFIABLE"
        printSolution bindings
