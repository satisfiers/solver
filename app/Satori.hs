module Main where

import CNF
import Control.Monad
import Control.Applicative
import Control.Monad.ST
import Control.Exception.Base
import Data.Either
import Text.Printf
import Solver
import Strategy
import Strategy.Impl
import Utils
import Data.List.Split
import qualified Data.Vector.Unboxed as U

printSolution :: U.Vector Int -> IO ()
printSolution bindings = do
  U.mapM_ (putStr . printf "%d ") (U.tail bindings)
  putStrLn " 0"

readInput :: IO [CNF]
readInput = do
  getLine
  contents <- filter (not . null) . map words . lines <$> getContents
  let separated = tail (splitWhen (\line -> head line == "p") contents)
      buildCNF = fromIntsCNF . map (init . map read)
      cnfs = map buildCNF separated
  return cnfs

type ConfigType s = DLAState DVSIDS LInactiveRemoval ARestarts s

main :: IO ()
main = do
    cnfs <- readInput
    forM_ cnfs $ \cnf -> do
      let solution = solve cnf (initConfig :: CNF -> ST s (ConfigType s))
      case solution of
        Unsat -> putStrLn "UNSAT"
        Sat bindings -> do
          putStrLn "SAT"
          printSolution bindings
