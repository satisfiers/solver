module Main where

import CNF
import Control.Monad
import Control.Applicative
import Control.Monad.ST
import Control.Exception.Base
import Data.Either
import Text.Printf
import Solver
import Strategy
import Strategy.Impl
import Utils
import qualified Data.Vector.Unboxed as U

printSolution :: U.Vector Int -> IO ()
printSolution bindings = do
  putStr "v "
  U.mapM_ (putStr . printf "%d ") (U.tail bindings)
  putStrLn ""

type ConfigType s = DLAState DVSIDS LInactiveRemoval ARestarts s

main :: IO ()
main = do
    cnf <- readCNF
    let solution = solve cnf (initConfig :: CNF -> ST s (ConfigType s))

    case solution of
      Unsat -> putStrLn "s UNSATISFIABLE"
      Sat bindings ->
        let eitherBoolCNF = checkBindings cnf bindings
        in case eitherBoolCNF of
              Left _ -> error "Nie zwrócono modelu dla wszystkich zmiennych"
              Right boolCNF -> do
                putStrLn "S SATISFIABLE"
                printSolution bindings
                putStr $ assert (isTrue boolCNF) ""
