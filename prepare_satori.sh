#!/bin/bash -

rm -fr satori.zip

stack clean
stack build

cp $(stack path --local-install-root)/bin/solver ./Satori/solver
zip -j satori.zip Satori/Makefile Satori/solver
rm -fr Satori/solver
