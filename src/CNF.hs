{-# LANGUAGE GeneralizedNewtypeDeriving
           , FlexibleInstances
           , MultiParamTypeClasses
           , BangPatterns #-}

module CNF where

import qualified Data.IntMap as IM
import Control.Monad.State
import Data.Maybe
import qualified Data.List as L
import Data.Foldable (foldrM, find)
import Data.Set (toList, fromList)
import qualified Data.Set as S
import Data.Either
import Data.Bits (xor)
import qualified Data.Vector.Unboxed as U
import Data.Vector.Unboxed ((!))

type Variable = Int

newtype Literal = Lit { unLit :: Int } deriving (Ord, Eq, Enum)

var :: Literal -> Int
var (Lit v) = abs v

-- | zwraca True jeśli niezanegowany
sign :: Literal -> Bool
sign (Lit v) | v >= 0    = True
             | otherwise = False

-- | Negacja literału
neg :: Literal -> Literal
neg (Lit v) = Lit (-v)

-- | Typ klauzuli
type Clause = [Literal]
-- | Typ CNFa
type CNF = [Clause]

type ClauseID = Int

type BoolClause = [Bool]
type BoolCNF = [BoolClause]

class BoolCo a where
    isTrue :: a -> Bool

instance BoolCo BoolClause where
    isTrue clause = True `elem` clause

instance BoolCo BoolCNF where
    isTrue = null . dropWhile isTrue

instance {-# OVERLAPPING #-} Show Literal where
    show lit | sign lit  = show (abs $ unLit lit)
             | otherwise = '~':show (abs $ unLit lit)

instance {-# OVERLAPPING #-} Show Clause where
    show literals = ("(" ++) . printLiterals literals $ ")"
      where printLiterals []      = ("" ++)
            printLiterals (x:xs)  = foldl printLiteral (show x ++) xs

            printLiteral pref lit = pref . ((" v " ++ show lit) ++)

instance {-# OVERLAPPING #-} Show CNF where
    show [] = ""
    show (c:cls) = foldl printClause (show c ++) cls ""
      where printClause pref c = pref . ((" & " ++ show c) ++)

type Bindings = U.Vector Int
data Solution = Unsat | Sat { assignement :: !Bindings }
              deriving (Eq, Show)

-- klasa reprezentuje model, w którym dla jakiegoś elementu
-- dostajemy Just <bool> jeśli przypisanu mu wartość
class Model b e where
    -- b - typ modelu, e - typ elementu
    for :: b -> e -> Maybe Bool
    -- zwraca True jeśli element jest prawdą w danym model
    isAssumed :: b -> e -> Bool
    isAssumed binds el = case binds `for` el of
                           Nothing    -> False
                           someValue  -> fromJust someValue
    -- zwraca True jeśli element nie jest łaszem w danym modelu
    isNotFalse :: b -> e -> Bool
    isNotFalse binds el = case binds `for` el of
                            Nothing   -> True
                            someValue -> fromJust someValue

instance Model Bindings Int where
    bindings `for` i = getValue (bindings ! i)
      where getValue val | val < 0   = Just False
                         | val > 0   = Just True
                         | otherwise = Nothing

instance Model Bindings Literal where
    bindings `for` lit = let varValue = bindings `for` var lit
                             in (== sign lit) <$> varValue

toBind :: Maybe Bool -> Int
toBind (Just isTrue) = if isTrue then 1 else -1
toBind Nothing       = 0

-- zakłada, że literał jest prawdziwy, zwraca binding dla zmiennej
assume :: Literal -> Int
assume (Lit val) | val > 0 = 1
                 | val < 0 = -1
                 | otherwise = error "There should be no 0 variable"


-- | Zwraca wartość literału po przypisaniu wartości jego zmiennej
assignVar :: Literal -> Bool -> Bool
assignVar lit value = not (xor (sign lit) value)

fromInts :: [Int] -> Clause
fromInts = map Lit

fromIntsCNF :: [[Int]] -> CNF
fromIntsCNF = map fromInts

emptyClause :: Clause
emptyClause = []

-- | Usuwa z problemu puste klauzule
removeEmpty :: CNF -> CNF
removeEmpty = filter (not . null)

-- | Usuwa zduplikowane literały z klauzul
removeDups :: CNF -> CNF
removeDups = map (S.toList . S.fromList)

-- | Usuwa klauzule z literałami i ich negacjami
removeTrivials :: CNF -> CNF
removeTrivials = filter isTrivial
  where isTrivial clause = let literals = S.fromList clause
                               hasNegation el = S.member (neg el) literals
                            in isNothing $ find hasNegation literals

variablesClause :: Clause -> S.Set Int
variablesClause clause = fromList $ map var clause

variablesCNF :: CNF -> S.Set Int
variablesCNF clauses = S.unions $ map variablesClause clauses

hasEmptyClause :: CNF -> Bool
hasEmptyClause = isJust . L.find (emptyClause ==)

checkClause :: Clause -> Bindings -> Either [Int] BoolClause
checkClause literals bindings
    | null errs   = Right oks
    | otherwise   = Left errs
    where checkLiteral l = let v = var l
                           in case bindings `for` v of
                                 Nothing -> Left v
                                 Just val -> Right (assignVar l val)
          (errs, oks) = partitionEithers (map checkLiteral literals)

checkBindings :: CNF -> Bindings -> Either [Int] BoolCNF
checkBindings clauses bindings = makeUnique $ foldr handleClause (Right []) clauses
  where handleClause cl (Left errs) =
          case checkClause cl bindings of
                  Left err -> Left $ err ++ errs
                  _        -> Left errs
        handleClause cl (Right bools) =
          case checkClause cl bindings of
                  Left err  -> Left err
                  Right oks -> Right $ oks : bools
        makeUnique (Right bools) = Right bools
        makeUnique (Left errs) = Left $ toList . fromList $ errs
