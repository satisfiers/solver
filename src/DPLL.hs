module DPLL where

import CNF
import UP
import Data.Maybe
import qualified Data.Set as S
import Control.Applicative
import qualified Data.Vector.Unboxed as U
import Data.Vector.Unboxed ((//))

dpllUP :: CNF -> Solution
dpllUP cnf =
    case dpll cnf of
      Nothing -> Unsat
      Just literals ->
        let
          size = (+1) . maximum . map var $ literals
          updates = map (\(Lit v) -> (abs v, v)) literals
          assignement = U.replicate size 0 // updates
        in Sat assignement
    where
      findVar :: CNF -> Maybe Int
      findVar cnf = let literals = concat cnf in
        if null literals then Nothing
                         else Just $ var (head literals)

      dpll :: CNF -> Maybe [Literal]
      dpll cnf = case findVar cnf of
        Nothing -> Just []
        Just v ->
          let (newCNFPos, assignedPos) = unitPropagation $ [Lit v]:cnf
              (newCNFNeg, assignedNeg) = unitPropagation $ [Lit (-v)]:cnf
              posSolution = if hasEmptyClause newCNFPos
                              then Nothing
                              else (++) (Lit v:assignedPos) <$> dpll newCNFPos
              negSolution = if hasEmptyClause newCNFNeg
                              then Nothing
                              else (++) (Lit (-v):assignedNeg) <$> dpll newCNFNeg
          in posSolution <|> negSolution
