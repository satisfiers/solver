{-# LANGUAGE RankNTypes
           , MultiParamTypeClasses
           , FlexibleInstances
           , BangPatterns
 #-}

module Monad ( runStErrContSTMonad
             , evalStErrContSTMonad
             , StErrContSTMonad
             , liftST
             ) where

import Control.Monad
import Control.Monad.ST.Strict
import Control.Monad.Except
import Control.Monad.State.Class
import Control.Monad.Cont.Class
import Control.Applicative


-- | monada kombinująca w sobie monadę stanową, either, ST oraz kontynuacje
-- głównie z powodów wydajnościowych, tak jak opisano tutaj:
-- <https://wiki.haskell.org/Performance/Monads>
-- >>= nie sprawdza czy nastąpił błąd - pozwala to zaoszczędzić dużą liczbę
-- ifów. Z błędem radzimy sobie explicite poprzez catchError albo przy
-- użyciu MonadPlus
newtype StErrContSTMonad e st s a =
  StErrContSTMonad { unStErrContSTMonad :: forall r. ((a -> (st -> ST s (Either e r, st)))
                                            -> (st -> ST s (Either e r, st))) }

runStErrContSTMonad :: StErrContSTMonad e st s a -> st -> ST s (Either e a, st)
runStErrContSTMonad m = unStErrContSTMonad m (\a st -> return (return a, st))

evalStErrContSTMonad :: StErrContSTMonad e st s a -> st -> ST s (Either e a)
evalStErrContSTMonad m s = do
    (result, _) <- runStErrContSTMonad m s
    return result

{-# INLINE bindMonad #-}
bindMonad m f = StErrContSTMonad $ \k -> unStErrContSTMonad m (\ !a -> unStErrContSTMonad (f a) k)

instance Monad (StErrContSTMonad e st s) where
    return x = StErrContSTMonad ($ x)
    (>>=) = bindMonad


instance Functor (StErrContSTMonad e st s) where
    fmap f m = do
      res <- m
      return (f res)


instance Applicative (StErrContSTMonad e st s) where
    pure = return
    mf <*> ma = do
      f <- mf
      a <- ma
      return (f a)


instance MonadState st (StErrContSTMonad e st s) where
    get = StErrContSTMonad $ \k st -> k st st
    put newSt = StErrContSTMonad $ \k _ -> k () newSt


instance MonadError e (StErrContSTMonad e st s) where
    throwError err = StErrContSTMonad $ \k st -> return (Left err, st)

    catchError act handler = StErrContSTMonad $ \k st ->
          do (result, newSt) <- runStErrContSTMonad act st
             case result of
               Right ok -> k ok newSt
               Left err -> unStErrContSTMonad (handler err) k newSt


instance Alternative (StErrContSTMonad e st s) where
    empty = return undefined
    m <|> n = m `mplus` n


-- zauważmy że mzero odrzuca kontyunację, co jest równoznaczne przerwaniu
-- obliczeń
instance MonadPlus (StErrContSTMonad e st s) where
    mzero = StErrContSTMonad $ \k st -> return (Left undefined, st)

    m `mplus` n = StErrContSTMonad $
                    \k st -> do (result, newSt) <- runStErrContSTMonad m st
                                case result of
                                  Right ok -> k ok newSt
                                  Left _ -> unStErrContSTMonad n k newSt


{-# INLINE liftST #-}
liftST :: ST s a -> StErrContSTMonad e st s a
liftST stmon = StErrContSTMonad $
          \k !st -> do a <- stmon
                       k a st

-- | MonadCont - problem z typowaniem
-- instance MonadCont (StErrContSTMonad e st s) where
--     callCC f = StErrContSTMonad $ \k -> unStErrContSTMonad (f (\a -> StErrContSTMonad (\_ -> k a))) k
