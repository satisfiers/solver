{-# LANGUAGE RankNTypes
           , BangPatterns
  #-}

module Solver (
              solve
              ) where

import CNF
import Monad
import Types
import UP
import Utils
import Strategy (Strategy)
import qualified Strategy
import Control.Monad.ST
import Control.Monad.State
import Control.Monad.Except
import Data.Either
import Data.Maybe
import Data.STRef
import qualified Data.Foldable as F
import qualified Data.List as L
import qualified Data.Sequence as S
import Data.Sequence ((|>), (<|))
import qualified Data.IntMap as IM
import qualified Data.Set as Set
import qualified Data.Vector.Unboxed.Mutable as UV
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Mutable as MV
import qualified Data.Vector as V
import Debug.Trace

-- | zakłada, że nie ma klauzul pustych albo pojedynczych oraz klauzul
-- trywialnie spełnionych
initWatches :: CNF -> Int -> ST s (TwoWatches s, TwoWatches s)
initWatches cnf varCount = do
    positives <- MV.replicate (varCount + 1) S.empty
    negatives <- MV.replicate (varCount + 1) S.empty
    forM_ (zip cnf [1, 2 .. ]) $ \(clause, id) ->
      case clause of
        []  -> error "There should be no empty clauses during intialization"
        [l] -> error "There should be no unit clauses during initialization"
        c   -> do let lit1 = c !! 0; lit2 = c !! 1
                      var1 = var lit1; var2 = var lit2
                      arr1 = if sign (c !! 0) then positives else negatives
                      arr2 = if sign (c !! 1) then positives else negatives
                  refWatch <- newSTRef (lit1, lit2)
                  MV.modify arr1 ((<|) (c, id, refWatch)) var1
                  MV.modify arr2 ((<|) (c, id, refWatch)) var2

    return (positives, negatives)

-- inicjalizuje stan solvera mając podany stan heurystyki
initSolverState :: Strategy c => CNF -> [Literal] -> c s -> ST s (SolverState c s)
initSolverState cnf trueLits config = do
    let maxCnf        = Set.findMax . variablesCNF $ cnf
        maxTrueLits   = if null trueLits then 0
                                         else Set.findMax . Set.fromList . map var $ trueLits
        varCount      = max maxCnf maxTrueLits
        clausesCount  = length cnf
    (positives, negatives) <- initWatches cnf varCount
    positivesLearn <- MV.replicate (varCount + 1) S.empty
    negativesLearn <- MV.replicate (varCount + 1) S.empty
    bindings <- UV.replicate (varCount + 1) 0 :: ST s (MBindings s)
    forM_ trueLits $ \lit -> -- zapisanie wartości znanych literałów od początku
      UV.write bindings (var lit) (assume lit)
    UV.write bindings 0 100 -- na wszelki wypadek, żeby przypadkiem nie złapać
    levels <- UV.replicate (varCount + 1) noLevel :: ST s (VarLevels s)

    return SS { formula    = cnf
              , trail      = trueLits  -- od razu ustawimy koniec traila na ostatnie
              , rootLevel  = 0
              , decLevel   = 0
              , posWatches = positives
              , negWatches = negatives
              , posLearnts = positivesLearn
              , negLearnts = negativesLearn
              , values     = bindings
              , levels     = levels
              , reasons    = IM.empty
              , propQue    = S.empty
              , idGen      = clausesCount
              , config     = config }


isSolved :: SolverM c s Bool
isSolved =  ((==) . length <$> gets trail) <*> variablesCountM

-- jedyna funkcja używana na zewnątrz, oczekuje funkcji, która wygeneruje
-- stan strategii, powinno to być initConfig :: CNF -> ST s (c s)
solve :: Strategy c => CNF -> (CNF -> forall s. ST s (c s)) -> Solution
solve cnf strategyInit = runST $
       case simplifyCNF cnf of -- uproszczenie problemu
         Nothing   -> return Unsat
         Just (cnf', trueLits) -> do strategyConf <- strategyInit cnf
                                     state <- initSolverState cnf' trueLits strategyConf
                                     result <- evalSolverM coreSolver state
                                     case result of
                                       Left _    -> return Unsat
                                       Right sol -> return sol

debugWatches :: SolverM c s ()
debugWatches = do
    posW <- gets posWatches
    negW <- gets negWatches
    posL <- gets posLearnts
    negL <- gets negLearnts
    varC <- variablesCountM
    forM_ [1,2 .. varC] $ \v -> do
      traceM ("Watches for: " ++ show (Lit (-v)))
      w <- liftST $ MV.read posW v
      traceShowM (map (\(clause, _, _) -> clause) (F.toList w))

      traceM ("Watches for: " ++ show (Lit v))
      w <- liftST $ MV.read negW v
      traceShowM (map (\(clause, _, _) -> clause) (F.toList w))


    return ()

debugPrintState :: SolverM c s ()
debugPrintState = do
    st <- get
    vals <- pure (values st) >>= liftST . U.unsafeFreeze
    levs <- pure (levels st) >>= liftST . U.unsafeFreeze
    traceM "==== Solver State ==== "
    traceM ("CNF: " ++ show (formula st))
    traceM ("DecLevel: " ++ show (decLevel st))
    traceM ("Trail: " ++ show (trail st))
    traceM ("Bindings: " ++ show vals)
    traceM ("Levels: " ++ show levs)
    traceM ("Queue: " ++ show (propQue st))
    debugWatches
    traceM ""
    return ()

-- właściwy solver i jego niezmienny rdzeń
-- reszta jest zależna od konkretnych implementacji strategii
-- zakłada, że cały stan został odpowiednio zainicjalizowany
coreSolver :: Strategy c => SolverM c s Solution
coreSolver = (do
    {-debugPrintState-}
    runQueue  -- przeprowadź propagację

    toRemove <- Strategy.reduceDB
    forM_ toRemove removeClauses -- jeśli Nothing to removeCluases nie zostanie uruchomione

    -- zapytaj strategię o kolejną akcję
    action <- Strategy.nextAction
    when (action == Restart)
        restartSolver
    Strategy.handleAction action

    solved <- isSolved
    if solved then do
        vals <- gets values
        bindings <- liftST $ U.freeze vals
        return $ Sat (U.imap (*) bindings)
    else do
        lit <- Strategy.decide
        modify $ \s -> s{ decLevel = decLevel s + 1 } -- kolejny poziom decyzyjny
        enqueue lit (emptyClause, -1)
        coreSolver                            -- w przypadku sukcesu jedziemy dalej

     ) `catchError` \err@(lit, clause, cid) -> do  -- analiza konfliktu
        modify (\s -> s{ propQue = S.empty }) -- czyścimy kolejkę

        curLevel <- gets decLevel; root <- gets rootLevel
        when (curLevel == root)
            (throwError err)    -- błąd już na poziomie 0

        (newClause, dl) <- Strategy.analyse err
        cancelUntil (max dl root) >>= Strategy.handleUndo -- wycofanie zmian do poziomu max(dl, root) "backtrack"

        newCid <- learnClause newClause
        Strategy.handleNewClause newClause newCid
        findUnit newClause >>= (`enqueue` (newClause, newCid)) -- zakolejkuj unitowy lieterał

        coreSolver

-- uaktualnia generator indentyfikatorów i zwraca nowy identyfikator
newClauseId :: SolverM c s Int
newClauseId =  do
    (newGen, id) <- gets $ nextId . idGen
    modify $ \s -> s{ idGen = newGen }
    return id

currentValue :: Literal -> SolverM c s (Maybe Bool)
currentValue lit = do
    vals <- gets values
    val <- liftST $ UV.read vals (var lit)
    if val == 0 then return Nothing
                else return $ Just (sign lit == (val > 0))

getWatchesArr :: Literal -> SolverM c s (TwoWatches s)
getWatchesArr lit | sign lit = gets negWatches
                  | otherwise = gets posWatches

getLearntsArr :: Literal -> SolverM c s (TwoWatches s)
getLearntsArr lit | sign lit = gets negLearnts
                  | otherwise = gets posLearnts

insertWatches :: Literal -> [Watch s] -> Bool -> SolverM c s ()
insertWatches lit watches learnt = do
    watchesArr <- if learnt then getLearntsArr lit else getWatchesArr lit
    liftST $! MV.modify watchesArr (S.>< (S.fromList watches)) (var lit)

findUnit :: Clause -> SolverM c s Literal
findUnit clause = do
    frozenAssigns <- gets values >>= (liftST . U.unsafeFreeze)
    let unassigned = filter (isNotFalse frozenAssigns) clause
    case unassigned of
      []    -> error "W nowo nauczonej klauzuli powinien istnieć niezawartościowany literał"
      [lit] -> if isAssumed frozenAssigns lit then error "Literał nie może być zawartościowany"
                                              else return lit
      lits  -> error "Zbyt wiele niefałszywych literałów"

-- | Dodaje nową klauzulę do bazy i zwraca jej id
learnClause :: Clause -> SolverM c s ClauseID
learnClause clause = do
    cid <- newClauseId
    case clause of
      x:y:lits -> do !ref <- liftST $ newSTRef (x, y)
                     let newWatch = (clause, cid, ref)
                     insertWatches (neg x) [newWatch] True
                     insertWatches (neg y) [newWatch] True
      other     -> return () -- puste i pojedyncze klauzule ignorujemy,
                             -- unity od razu się zakolejkują
    return cid

-- | usuwa ze zbioru nauczonych klauzul
removeClauses :: [ClauseID] -> SolverM c s ()
removeClauses clausesIds = do
    let idsSet = Set.fromList clausesIds
        filterClauses = S.filter (\(_, id, _) -> not (Set.member id idsSet))

    varsNum <- variablesCountM
    !oldPos <- gets posLearnts >>= liftST . V.unsafeFreeze
    !oldNeg <- gets negLearnts >>= liftST . V.unsafeFreeze
    newPos <- liftST $! V.unsafeThaw (V.map filterClauses oldPos)
    newNeg <- liftST $! V.unsafeThaw (V.map filterClauses oldNeg)

    modify $ \s -> s{ posLearnts = newPos
                    , negLearnts = newNeg }

-- | Resetuje solver bez usuwania nauczonych klauzul
restartSolver :: SolverM c s ()
restartSolver = gets rootLevel >>= cancelUntil >>= const (return ())

-- | zwraca element z początku kolejki
top :: SolverM c s Literal
top = gets $ (`S.index` 0) . propQue

-- | zrzuca element z początku kolejki
pop :: SolverM c s ()
pop = modify $ \s -> let !newQue = S.drop 1 (propQue s)
                         in s{ propQue = newQue }

-- | wrzuca element na koniec kolejki
push :: Literal -> SolverM c s ()
push lit = modify $ \s -> let !newQue = propQue s |> lit
                         in s{ propQue = newQue }

-- | oznacza literał jako True, aktualizuje stan
assign :: Literal -> (Clause, ClauseID) -> SolverM c s ()
assign lit from = do
    let varl = var lit
    -- aktualizacja wartościowania zmiennej
    binds <- gets values
    liftST $! UV.write binds varl (assume lit)

    -- zaktualizuje powód dla zmiennej, jeśli powód nie jest pusty
    unless (null from)
        (modify $ \s -> s{ reasons = IM.insert varl from (reasons s) })
    -- aktualizacja traila
    modify $ \s -> s{ trail = lit:trail s}
    -- zapis poziomu decyzyjnego literału
    levs <- gets levels
    currentDL <- gets decLevel
    liftST $! UV.write levs varl currentDL


-- | Usuwa wartościowanie literałów do podanego poziomu
-- Zwraca listę odwartościowanych literałów
cancelUntil :: Int -> SolverM c s [Literal]
cancelUntil level = do
    !frozenLevels <- gets levels >>= liftST . U.unsafeFreeze
    oldTrail <- gets trail

    let (toRemove, newTrail) = span (\lit -> frozenLevels U.! var lit > level) oldTrail

    values <- gets values;
    levels <- gets levels;
    reasons <- gets reasons
    forM_ toRemove
        (unassign values levels reasons)

    modify $ \s -> s{ trail = newTrail   -- aktualizacja traila
                    , decLevel = level } -- przywrócenie poziomu

    return toRemove

-- | Usuwa wartościowanie literału
unassign :: MBindings s -> VarLevels s -> ReasonMap -> Literal -> SolverM c s ()
unassign values levels reasons lit = do
    let varl = var lit
    liftST $! UV.write values varl 0
    liftST $! UV.write levels varl noLevel
    modify $ \s -> s{ reasons = IM.delete varl reasons }

-- dodaje literał do kolejki propagacji i zwraca zaktualizowaną kolejkę
-- w przypadku napotkania konfliktu zwraca error
enqueue :: Literal -> (Clause, ClauseID) -> SolverM c s ()
enqueue lit from = do
    currVal <- currentValue lit
    case currVal of
      Nothing    -> do assign lit from
                       push lit
      Just True  -> return ()
      Just False -> throwError (lit, fst from, snd from)

-- propaguje pojedynczą klauzulę, uaktualnia kolejkę jeśli to konieczne
propagateClause :: Literal -> Bool -> (Watch s, [Watch s]) -> SolverM c s ()
propagateClause lit learnt (watch@(c, id, ref), rest) = do
    (first, second) <- liftST $! readSTRef ref
    values <- gets values
    frozenValues <- liftST $! U.unsafeFreeze values
    let otherLit = if first == neg lit then second else first
    otherVal <- currentValue otherLit
    -- klauzula już jest spełniona
    if isAssumed frozenValues otherLit
      then insertWatches lit [watch] learnt -- wkładamy z powrotem do obserwowanych
    else
      case L.find (\l -> l /= neg lit && l /= otherLit && isNotFalse frozenValues l) c of
        -- należy zaktualizować watche na nowo znaleziony literał
        Just newLit -> do liftST $! writeSTRef ref (otherLit, newLit)
                          insertWatches (neg newLit) [watch] learnt
        -- klauzula jest unitem
        Nothing     -> do insertWatches lit [watch] learnt
                          enqueue otherLit (c, id)
                          -- jeśli błąd przywracamy stare watche
                          `catchError` \err -> do insertWatches lit rest learnt
                                                  throwError err

-- dokonuje propagacji, zwraca listę literałów ustawionych na True
propagate :: Literal -> SolverM c s ()
propagate lit = do
    queue <- enqueue lit (emptyClause, -1)
    runQueue

-- uruchamia propagacje dopóki kolejka nie będzie pusta
runQueue :: SolverM c s ()
runQueue = do
    que <- gets propQue
    unless (null que) $ do
      lit <- top
      pop

      watches <- getWatchesArr lit
      learnts <- getLearntsArr lit
      watchedClauses <- liftST $! F.toList <$> MV.read watches (var lit)
      learntClauses  <- liftST $! F.toList <$> MV.read learnts (var lit)
      -- usuwa watche dla tego literału
      liftST $! MV.write watches (var lit) S.empty
      liftST $! MV.write learnts (var lit) S.empty

      forM_ (zip watchedClauses (L.tails watchedClauses)) (propagateClause lit False)
      forM_ (zip learntClauses (L.tails learntClauses)) (propagateClause lit True)
      runQueue
