{-# LANGUAGE BangPatterns #-}
module Strategy where

import CNF
import Types hiding (SolverState(..))
import qualified Types as T (SolverState(..))
import Monad
import Control.Monad.State
import Control.Monad.ST
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Unboxed.Mutable as UV


data StrategyState c s = StrSt
    { formula :: CNF                -- ^ oryginalna formuła
    , trail :: ![Literal]            -- ^ stos kolejno przypisanych literałów
    , rootLevel :: !Int              -- ^ poziom za który nie można się cofnąć
    , decLevel :: !Int               -- ^ aktualny decision level
    , values :: !FrozenBindings         -- ^ obecne przypisanie wartości literałom
    , levels :: !FrozenVarLevels         -- ^ poziom na którym zmienna została przypisana
    , reasons :: !ReasonMap          -- ^ mapuje zmienną na klauzulę wymuszającą jej wartość
    , config :: c s                 -- ^ konfiguracja dla @Strategy, s jest konieczny gdy mamy do czynienia
                                    --   z operacjami na ST w configu
    }

getStrategyState :: SolverM c s (StrategyState c s)
getStrategyState = do
    st <- get
    frozenVals <- liftST . U.unsafeFreeze $ T.values st
    frozenLevels <- liftST . U.unsafeFreeze $ T.levels st
    return StrSt { formula    = T.formula st
                 , trail      = T.trail st
                 , rootLevel  = T.rootLevel st
                 , decLevel   = T.decLevel st
                 , values     = frozenVals
                 , levels     = frozenLevels
                 , reasons    = T.reasons st
                 , config     = T.config st }

class Strategy a where

    -- | inicjalizuje konfigurację strategii
    initConfig :: CNF -> ST s (a s)

    -- | zwraca kolejny literał, który należy zawartościować na True,
    -- jeśli taki istnieje
    decide :: SolverM a s Literal

    -- | dla napotkanego konfliktu zwraca nowo nauczoną klauzulę oraz nowy
    -- poziom decyzyjny - nie zmienia stanu solvera, co najwyżej swój stan
    analyse :: SolverError -> SolverM a s (Clause, Int)

    -- | funkcja wywołana gdy solver usunął wartościowanie podanych literałów
    handleUndo :: [Literal] -> SolverM a s ()

    -- | uaktualnia swoją bazę dla nowej klauzuli
    handleNewClause :: Clause -> ClauseID -> SolverM a s ()

    -- | zwraca listę nauczonych klauzul do usunięcia z bazy, jeśli uzna to
    -- za konieczne
    reduceDB :: SolverM a c (Maybe [ClauseID])

    -- | zwraca akcję którą ma podjąć solver po ostatnio udanej propagacji
    nextAction :: SolverM a s SolverAction

    -- | uaktualnie swój stan przed podjęciem akcji przez solver
    handleAction :: SolverAction -> SolverM a s ()

-- | Monada specjalnie stworzona dla mieszanych strategii, żeby ich pisanie było
-- trochę przyjemniejsze
type StrategyM c s = StateT (StrategyState c s) (ST s)

class DecideStrategy a where
    initConfigD :: CNF -> ST s (a s)

    decideD :: StrategyM a s Literal

    handleNewClauseD :: Clause -> ClauseID -> StrategyM a s ()
    handleNewClauseD _ _ = return ()

    -- | funkcja wywołana gdy solver usunął wartościowanie podanych literałów
    handleUndoD :: [Literal] -> StrategyM a s ()
    handleUndoD = const $ return ()

class LearningStrategy a where
    initConfigL :: CNF -> ST s (a s)

    analyseL :: SolverError -> StrategyM a s (Clause, Int)

    handleNewClauseL :: Clause -> ClauseID -> StrategyM a s ()
    handleNewClauseL _ _ = return ()

    reduceDBL :: StrategyM a s (Maybe [ClauseID])

    -- | funkcja wywołana gdy solver usunął wartościowanie podanych literałów
    handleUndoL :: [Literal] -> StrategyM a s ()
    handleUndoL = const $ return ()

class ActionStrategy a where
    initConfigA :: CNF -> ST s (a s)

    handleUndoA :: [Literal] -> StrategyM a s ()
    handleUndoA = const $ return ()

    handleNewClauseA :: Clause -> ClauseID -> StrategyM a s ()
    handleNewClauseA _ _ = return ()

    nextActionA :: StrategyM a s SolverAction

    handleActionA :: SolverAction -> StrategyM a s ()

data DLAState d l a s = DLA
                      { decideSt:: d s
                      , learnSt :: l s
                      , actionSt :: a s
                      }

-- | Wykonuje akcje w monadzie StrategyM d s r, a następnie zwraca akcję
-- w monadzie SolverM z odpowiednio zaktualizowanym stanem i tym samym
-- wynikiem co akcja strategii
{-# INLINE runActionD #-}
runActionD :: StrategyM d s r -> SolverM (DLAState d l a) s r
runActionD strat = do
    st <- get
    strSt <- getStrategyState
    let conf = config strSt
        dconf = decideSt conf
    (res, newConf) <- liftST $ runStateT strat strSt { config = dconf }
    put $ st{ T.config = conf { decideSt = config newConf }}
    return res

-- | Wykonuje akcje w monadzie StrategyM l s r, a następnie zwraca akcję
-- w monadzie SolverM z odpowiednio zaktualizowanym stanem i tym samym
-- wynikiem co akcja strategii
{-# INLINE runActionL #-}
runActionL :: StrategyM l s r -> SolverM (DLAState d l a) s r
runActionL strat = do
    st <- get
    strSt <- getStrategyState
    let conf = config strSt
        lconf = learnSt conf
    (res, newConf) <- liftST $ runStateT strat strSt { config = lconf }
    put $ st{ T.config = conf { learnSt = config newConf }}
    return res

-- | Wykonuje akcje w monadzie StrategyM a s r, a następnie zwraca akcję
-- w monadzie SolverM z odpowiednio zaktualizowanym stanem i tym samym
-- wynikiem co akcja strategii
{-# INLINE runActionA #-}
runActionA :: StrategyM a s r -> SolverM (DLAState d l a) s r
runActionA strat = do
    st <- get
    strSt <- getStrategyState
    let conf = config strSt
        aconf = actionSt conf
    (res, newConf) <- liftST $ runStateT strat strSt { config = aconf }
    put $ st{ T.config = conf { actionSt = config newConf }}
    return res

-- | Implementacja mieszanej strategii
instance (DecideStrategy d, LearningStrategy l, ActionStrategy a) => Strategy (DLAState d l a) where

    initConfig cnf = do
      dconf <- initConfigD cnf
      lconf <- initConfigL cnf
      aconf <- initConfigA cnf
      return DLA { decideSt = dconf
                 , learnSt = lconf
                 , actionSt = aconf }

    decide = runActionD decideD

    analyse err = runActionL (analyseL err)

    {-# INLINE handleUndo #-}
    handleUndo literals = do
      runActionD (handleUndoD literals)
      runActionL (handleUndoL literals)
      runActionA (handleUndoA literals)

    handleNewClause clause cid = do runActionL (handleNewClauseL clause cid)
                                    runActionD (handleNewClauseD clause cid)
                                    runActionA (handleNewClauseA clause cid)

    reduceDB = runActionL reduceDBL

    nextAction = runActionA nextActionA

    handleAction act = runActionA (handleActionA act)
