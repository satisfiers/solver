module Strategy.Impl where

import CNF
import Types hiding (SolverState(..))
import qualified Types as T (SolverState(..))
import Utils
import Strategy
import Data.Maybe
import System.Random
import Control.Monad.State
import Data.Foldable (foldlM)
import Data.Vector.Unboxed ((!))
import qualified Data.List as L
import Control.Exception.Base (assert)
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Unboxed.Mutable as UV
import qualified Data.IntMap as IM
import qualified Data.Set as Set

import Debug.Trace

-- | -------- DECISION STRATEGIES -------------------------------------------

-- ta strategia wybiera pierwszy nieustawiony terminal i losowo wybiera
-- jego wartość
newtype DFirstFree s = DFF StdGen
instance DecideStrategy DFirstFree where
    initConfigD cnf = return $ DFF (mkStdGen 0)

    decideD = do
      vals <- gets values
      DFF gen <- gets config
      case U.findIndex (0 ==) vals of
        Nothing -> error "Brak wolnych zmiennych do zawartościowania"
        Just v  -> do let (val, gen') = random gen
                      modify $ \s -> s { config = DFF gen' }
                      if val then return $ Lit v
                             else return $ Lit (-v)

-- | Dla obecnego wartościowania i generatora losowego zwraca
-- wybrane wartościowanie pewnego literału oraz nowy generator
makeRandomChoice :: FrozenBindings -> StdGen -> (Literal, StdGen)
makeRandomChoice bindings gen =
    let unassigned = unassignedVars bindings
        (ind, gen') = randomR (0, U.length unassigned - 1) gen
        (isTrue, gen'') = random gen'
    in if isTrue then (Lit (unassigned ! ind), gen'')
                 else (Lit (- unassigned ! ind), gen'')

-- wybór losowej wartości dla losowej wolnej zmiennej
newtype DRandomChoice s = DRC StdGen
instance DecideStrategy DRandomChoice where
    initConfigD cnf = return $ DRC (mkStdGen 0)

    decideD = do
      DRC gen <- gets config
      vals <- gets values
      let (lit, gen') = makeRandomChoice vals gen
      modify $ \s -> s { config = DRC gen' }
      return lit

-- TODO zamiast wybór randomowej wartości przypisz ostatnią
-- | VSIDS dla wyboru zmiennych
data DVSIDS s = DVSIDS
              { varActivities    :: UV.MVector s Double -- mapa na aktywności klauzul
              , varDecay         :: !Double           -- zmniejsza aktywność klauzul
              , varActThreshold  :: !Double           -- próg po którym przeskalowujemy aktywności
              , varInc           :: !Double           -- o ile zwiększamy aktywności
              , varRescaler      :: !Double           -- o ile reskalujemy zmienne
              , vsidsGen         :: !StdGen           -- generator losowy do wyboru znaku zmiennej
              , vsidsRandPercent :: !Double           -- procent decyzyjny przeprowadzonych losowo
              , vsidsVals        :: UV.MVector s Int  -- ostatnie przypisania
              }

varBumpActivities :: [Variable] -> StrategyM DVSIDS s ()
varBumpActivities vids = do
    conf <- gets config
    let acts = varActivities conf
        inc = varInc conf

        update big v = do
          oldAct <- UV.unsafeRead acts v
          let newAct = oldAct + inc
          UV.unsafeWrite acts v newAct
          return $ max big newAct

    biggest <- foldlM update 0 vids

    when (biggest > varActThreshold conf)
      varRescaleActivity

varDecayActivity :: StrategyM DVSIDS s ()
varDecayActivity = do
    conf <- gets config
    let decay  = varDecay conf
        oldInc = varInc conf
    modify $ \s -> s{ config = conf{ varInc = decay * oldInc } }

varRescaleActivity :: StrategyM DVSIDS s ()
varRescaleActivity = do
    conf <- gets config
    let acts = varActivities conf
        rescale = varRescaler conf
        varCount = pred (UV.length acts)
        oldInc = varInc conf
    forM_ [1,2 .. varCount] $ \v -> UV.modify acts (rescale *) v
    modify $ \s -> s{ config = conf{ varInc = oldInc * rescale } }

instance DecideStrategy DVSIDS where
    initConfigD cnf = do
      let varCount = Set.findMax . variablesCNF $ cnf
      activities <- UV.replicate (varCount + 1) 0
      vals <- UV.replicate (varCount + 1) 0
      return DVSIDS
            { varActivities    = activities
            , varDecay         = 1.5
            , varActThreshold  = 1000000
            , varInc           = 1
            , varRescaler      = 0.000001
            , vsidsGen         = mkStdGen 2
            , vsidsRandPercent = 2.0
            , vsidsVals        = vals
            }

    decideD = do
      conf <- gets config
      vals <- gets values
      let activities = (UV.tail . varActivities) conf
          vvals = vsidsVals conf
          valsTail = U.tail vals
          gen = vsidsGen conf
          (randChoice, gen') = randomR (0.0, 100.0) gen :: (Double, StdGen)
          update var val = do
            oldVal <- UV.unsafeRead vvals var
            UV.unsafeWrite vvals var (if val /= 0 then val else oldVal)
      U.imapM_ update vals

      if randChoice <= vsidsRandPercent conf
        then do -- wybór losowy
          let (lit, gen'') = makeRandomChoice vals gen'
          modify $ \s -> s { config = conf{ vsidsGen = gen''} }
          return lit
        else do -- wybór najbardziej aktywnego
          frozenActivities <- U.unsafeFreeze activities
          let aggregate agg@(bestInd, best) ind (val, act) | val /= 0 = agg
                                                           | act < best = agg
                                                           | otherwise = (ind, act)
              (bestInd, _) = U.ifoldl' aggregate (-1, 0) (U.zip valsTail frozenActivities)
              var = bestInd + 1 -- bo taile zmniejszyły indeksy o 1

          when (var == 0) (error "Nie znaleziono wolnej zmiennej")
          vvals <- gets (vsidsVals . config)
          lastBind <- UV.unsafeRead vvals var
          if lastBind == 0 then do
                                let (isTrue, gen'') = random gen'
                                    val = if isTrue then 1 else -1
                                UV.unsafeWrite vvals var val
                                modify $ \s -> s { config = conf{ vsidsGen = gen'' } }
                                return $ Lit (var * val)
          else do
            let val = lastBind
            return $ Lit (var * quot val (abs val))


    handleNewClauseD clause _ = do
      varBumpActivities (map var clause)
      varDecayActivity

-- | -------- LEARNING STRATEGIES -------------------------------------------

-- | Zwraca listę literałów których zawartościowanie na True doprowadziło
-- do konfliktu. Z podanej listy wyklucza literał dla zmiennej
{-# INLINE calculateReason #-}
calculateReason :: Clause -> Maybe Variable -> [Literal]
calculateReason clause variable =
    let clause' = if isJust variable then filter (\lit -> var lit /= fromJust variable) clause
                                     else clause
    in map neg clause'

-- | Zwraca nowo nauczoną klauzulę dla konfliktu, poziom decyzyjny na
-- który wracamy oraz liste konfliktujących klauzul
learnNewClause :: (Clause, ClauseID) -> StrategyM c s (Clause, Int, [ClauseID])
learnNewClause conflict@(conflictClause, conflictID) = do
    currentLevel <- gets decLevel
    varCount <- variablesCount <$> gets values
    tr <- gets trail
    frLevels <- gets levels
    seen <- UV.replicate (varCount + 1) False

    -- literały z ostatniego poziomu decyzyjnego, kolejka dla BFS przeglądania
    let lastLits = takeWhile (\lit -> frLevels U.! var lit == currentLevel) tr
        reasonHandler = handleReason currentLevel frLevels seen
        bfsConflicts = bfs currentLevel frLevels seen

    -- gdzieś w konfliktowej klauzuli znajduje się literał z ostatniego
    -- poziomu, trzeba go znaleźć dlatego Nothing
    firstResult <- reasonHandler (calculateReason conflictClause Nothing)
    -- uruchamiamy bfs bo kolejnych literałach - rozważamy tylko te jak
    -- dotąd widziane
    (learntLists, bcLevel', lastLit, badClauses) <- bfsConflicts lastLits
    when (var lastLit == 0)
      (error "Nie znaleziono literału z ostatniego poziomu podczas analizy")

    let (learntLists', bcLevel) = combineResults (learntLists, bcLevel') firstResult
    -- dodajemy ostatni analizowany literał do nowej klauzuli
    -- w ten sposób w nowo nauczonej klauzuli będzie tylko jeden literał
    -- z ostatniego poziomu decyzyjnego
    return (neg lastLit : concat learntLists', bcLevel, conflictID:badClauses)

    where combineResults (litsList, level) (newLits, newLevel) = (newLits:litsList, max level newLevel)

          {-# INLINE handleReason #-}
          handleReason :: Int -> FrozenVarLevels -> UV.MVector s Bool -> [Literal] -> StrategyM c s ([Literal], Int)
          handleReason curLevel frLevels seen lits = do
            root <- gets rootLevel
            let handleLit lit = do
                    wasSeen <- UV.read seen (var lit)
                    if not wasSeen then do
                      UV.write seen (var lit) True
                      let litLevel = frLevels ! var lit   -- ignorujemy literały z obecnego poziomu i z roota
                      if litLevel == curLevel || litLevel == root
                        then return Nothing
                        else return $ Just (neg lit, litLevel)
                    else return Nothing

                aggregate (learnt, bcLevel) lit = do
                    litResult <- handleLit lit
                    case litResult of
                      Nothing -> return (learnt, bcLevel)
                      Just (newLit, litLevel) -> return (newLit:learnt, max bcLevel litLevel)

            foldlM aggregate ([], root) lits

          -- funkcja idzie kolejno po kolejce literałów, dla każdego
          -- widzianego dotąd oblicza literały, które wymusiły jego wartość
          -- i wybiera z nich takie, z których po negacji zbudujemy nową
          -- klauzulę
          {-# INLINE bfs #-}
          bfs :: Int -> FrozenVarLevels -> UV.MVector s Bool -> [Literal] -> StrategyM c s ([[Literal]], Int, Literal, [Int])
          bfs curLevel frLevels seen tr = do
            root <- gets rootLevel
            let aggregate agg@(learntLists, level, lastLit, clauses) lit = do
                  wasSeen <- UV.read seen (var lit)
                  if not wasSeen then return agg -- niewidziane dotąd pomijamy
                  else do
                    reasonMap <- gets reasons
                    let (reasonClause, rid) = reasonMap IM.! var lit
                        lits = calculateReason reasonClause (Just $ var lit)
                    newResults <- handleReason curLevel frLevels seen lits
                    let (learntsLists', level') = combineResults (learntLists, level) newResults
                    return (learntsLists', level', lit, rid:clauses)

            foldlM aggregate ([], root, Lit 0 {- głupi literał na start -}, []) tr


-- | prosta strategia, która tylko zwraca nauczone klauzule, nie redukuje
-- ziobru nauczonych klauzul w ogóle
newtype LJustAnalyse s = LJA ()
instance LearningStrategy LJustAnalyse where
    initConfigL = const $ return (LJA ())

    analyseL err@(_, clause, cid) = do
      (lclause, dl, _) <- learnNewClause (clause, cid)
      return (lclause, dl)

    reduceDBL = return Nothing

-- | Strategia jak w Min Sacie, analizuje do FUIP i usuwa co jakis czas
-- niektóre nauczone klauzule
data LInactiveRemoval s = LIR
                        { claActivities    :: IM.IntMap Double -- mapa na aktywności klauzul
                        , claDecay         :: !Double           -- zmniejsza aktywność klauzul
                        , claActThreshold  :: !Double           -- próg po którym przeskalowujemy aktywności
                        , claInc           :: !Double           -- o ile zwiększamy aktywności
                        , claRescaler      :: !Double           -- o ile reskalujemy zmienne
                        , maxLearnts       :: !Int              -- maksymalna liczba nauczonych klauzul
                        , removeFraction   :: !Double           -- część nauczonych klauzul do usunięcia
                        }


claBumpActivities :: [ClauseID] -> StrategyM LInactiveRemoval s ()
claBumpActivities cids = do
    conf <- gets config
    let activities                = claActivities conf
        runUpdate (acts, big) cid = case IM.lookup cid acts of
                                      Nothing -> (acts, big)
                                      Just oldAct -> let newAct = oldAct + claInc conf
                                                         in (IM.insert cid newAct acts, max big newAct)
        (newActivities, biggest)  = foldl runUpdate (activities, 0) cids
    modify $ \s -> s{ config = conf{ claActivities = newActivities }}
    when (biggest > claActThreshold conf)
      claRescaleActivity

claDecayActivity :: StrategyM LInactiveRemoval s ()
claDecayActivity = do
    conf <- gets config
    let decay  = claDecay conf
        oldInc = claInc conf
    modify $ \s -> s{ config = conf{ claInc = decay * oldInc } }

claRescaleActivity :: StrategyM LInactiveRemoval s ()
claRescaleActivity = do
    conf <- gets config
    let oldActivities = claActivities conf
        rescale       = claRescaler conf
        newActivities = IM.map (rescale *) oldActivities
        oldInc = claInc conf
    modify $ \s -> s{ config = conf{ claActivities = newActivities
                                   , claInc = oldInc * rescale }}


instance LearningStrategy LInactiveRemoval where
    initConfigL cnf = return LIR { claActivities = IM.empty
                                 , claDecay = 1.5
                                 , claActThreshold = 1000000
                                 , claInc = 1
                                 , claRescaler = 0.00001
                                 , maxLearnts = quot (length cnf) 3  -- tak jest w minsacie
                                 , removeFraction = 0.5
                                 }

    analyseL err@(_, clause, cid) = do
      (lclause, dl, conflicted) <- learnNewClause (clause, cid)
      claBumpActivities conflicted
      return (lclause, dl)

    handleNewClauseL _ cid = do
      conf <- gets config
      let acts = claActivities conf
      modify $ \s -> s{config = conf{ claActivities = IM.insert cid 0 acts } }
      claBumpActivities [cid]
      claDecayActivity

    reduceDBL = do
      conf <- gets config
      vals <- gets values
      let learntCount = IM.size (claActivities conf)
          assigned   = assignedCount vals
      if learntCount - assigned <= maxLearnts conf
        then return Nothing
      else do
        let activities = claActivities conf
            actsList = L.sortOn snd (IM.toList activities)
            removeCount = floor $ removeFraction conf * fromIntegral (IM.size activities)
            (toRemove, toLeave) = splitAt removeCount actsList
        modify $ \s -> s{ config = conf{ claActivities = IM.fromList toLeave } }
        return $ Just (map fst toRemove)


-- | -------- ACTION STRATEGIES -------------------------------------------

newtype AAlwaysContinue s = AAC ()
instance ActionStrategy AAlwaysContinue where
    initConfigA = const (return $ AAC ())
    handleUndoA = const $ return ()
    nextActionA = return Continue
    handleActionA = const $ return ()

data ARestarts s = AR
                 { conflicts :: !Int
                 , confMult :: !Double
                 , maxConflicts :: !Int }

instance ActionStrategy ARestarts where
    initConfigA _ = return $! AR { conflicts = 0
                                 , confMult = 1.5
                                 , maxConflicts = 120 }

    handleUndoA = const $ return ()

    handleNewClauseA _ _ = do
      conf <- gets config
      modify $ \s -> s{ config = conf{ conflicts = conflicts conf + 1 } }

    nextActionA = do
      conf <- gets config
      let maxConfls = maxConflicts conf
      if conflicts conf >= maxConfls
        then do
          let mult = confMult conf
              maxC = round $ mult * fromIntegral maxConfls
          modify $ \s -> s{ config = conf{ conflicts = 0, maxConflicts = maxC }}
          return Restart
      else return Continue

    handleActionA = const $ return ()
