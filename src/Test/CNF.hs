module Test.CNF where

import CNF
import Test.QuickCheck
import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen
import Data.Set (toList, size)
import qualified Data.Vector.Unboxed as U
import Data.Vector.Unboxed ((//))
import Control.Monad

trueFalseIntersparse :: [Bool]
trueFalseIntersparse = True : False : trueFalseIntersparse

clauseGen :: Int -> Gen Clause
clauseGen maxSize = do
    size <- choose (1, maxSize)
    ints <- vectorOf size (choose (-100, 100))
    return $ fromInts ints

cnfGen :: Int-> Int -> Gen CNF
cnfGen maxClauses maxLiterals = do
    size <- choose (1, maxClauses)
    vectorOf size (clauseGen maxLiterals)

bindingsGen :: CNF -> Double -> Gen Bindings
bindingsGen cnf boundFraction = do
    let vars = variablesCNF cnf
        count = round (boundFraction * fromIntegral (size vars))
        maxVal = maximum vars
    chosenVars <- take count <$> shuffle (toList vars)
    bools <- vectorOf count (arbitrary :: Gen Bool)
    return $ U.replicate (maxVal +1) 0 // zip chosenVars (map (toBind . Just) bools)
