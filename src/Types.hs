{-# LANGUAGE BangPatterns #-}
module Types where

import CNF
import Monad

import qualified Data.Vector.Mutable as MV
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed.Mutable as UV
import qualified Data.Vector.Unboxed as U
import qualified Data.IntMap as IM
import qualified Data.Sequence as S
import Control.Monad.ST
import Data.STRef

type Watch s = (Clause, ClauseID, STRef s (Literal, Literal)) -- prócz klauzuli potrzebujemy znać jeszcze drugi literał
                                                    -- dla którego Watch będzie zawierać tę samą referencję

type TwoWatches s = MV.MVector s (S.Seq (Watch s))
type MBindings s = UV.MVector s Int
type FrozenBindings = U.Vector Int
type VarLevels s = UV.MVector s Int
type FrozenVarLevels = U.Vector Int
type ReasonMap = IM.IntMap (Clause, ClauseID)
type PropQue = S.Seq Literal

class IdGen a where
    init :: a
    nextId :: a -> (a, Int)

instance IdGen Int where
    init = 0
    nextId id = (succ id, id)

data SolverState c s = SS
    { formula :: CNF                -- ^ oryginalna formuła
    , trail :: ![Literal]           -- ^ stos kolejno przypisanych literałów
    , rootLevel :: !Int             -- ^ poziom za który nie można się cofnąć
    , decLevel :: !Int              -- ^ aktualny decision level
    , posWatches :: !(TwoWatches s) -- ^ watch array dla pozytywnych literałów
    , negWatches :: !(TwoWatches s) -- ^ watch array dla negatywnych literałów
    , posLearnts :: !(TwoWatches s) -- ^ watch array dla pozytywnych literałów tylko z nauczonymi klauzalami
    , negLearnts :: !(TwoWatches s) -- ^ watch array dla negatywnych literałów tylko z nauczonymi klauzalami
    , values :: !(MBindings s)      -- ^ obecne przypisanie wartości literałom
    , levels :: !(VarLevels s)      -- ^ poziom na którym zmienna została przypisana
    , reasons :: !ReasonMap         -- ^ mapuje zmienną na klauzulę wymuszającą jej wartość
    , propQue :: !PropQue           -- ^ kolejka propagacji
    , idGen :: !Int                 -- ^ generuje kolejne id klauzul
    , config :: c s                 -- ^ konfiguracja dla @Strategy, s jest konieczny gdy mamy do czynienia
                                    --   z operacjami na ST w configu
    }


-- reprezentuje akcje którą może podjąć solver po każdym udanym kroku
-- propagacji
data SolverAction = Continue
                  | Restart
                  | Fork  -- w przyszłosci - czy należy się zbranchować
                  deriving (Show, Eq, Ord)

-- poziom decyzyjny zmiennej która nie została jeszcze przypisana
noLevel :: Int
noLevel = -1

-- literał, który doprowadził do sprzeczności oraz klauzula która to
-- wymusiła
type SolverError = (Literal, Clause, ClauseID)

-- monadyczny solver
type SolverM c s = StErrContSTMonad SolverError (SolverState c s) s

runSolverM :: SolverM c s a -> SolverState c s -> ST s (Either SolverError a, SolverState c s)
runSolverM = runStErrContSTMonad

evalSolverM :: SolverM c s a -> SolverState c s -> ST s (Either SolverError a)
evalSolverM = evalStErrContSTMonad
