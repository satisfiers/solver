 {-# LANGUAGE RankNTypes #-}
 {-# LANGUAGE FlexibleContexts #-}
 {-# LANGUAGE GADTs #-}

module UP (unitPropagation) where

import CNF

import Control.Monad
import Data.STRef

import Control.Monad.ST
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Identity
import qualified Data.Sequence as Q
import Data.Sequence ((<|), (|>))
import qualified Data.Map.Strict as M
import qualified Data.Vector as V
import qualified Data.Set as S
import qualified Data.Vector.Mutable as VM
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Unboxed.Mutable as UM

-- | Formuła będzie wektorem klauzul będących wektorami literałów
type VecFormula = V.Vector (S.Set Literal)
type MVecFormula s = VM.MVector s (S.Set Literal)

-- | vec[i] == True <=> klauzula i-ta zostala spełniona
type MTrueClauses s = UM.MVector s Bool

-- | mapuje literał na numery klauzul w których się znajduje
type InClauses = M.Map Literal [Int]

type Queue = Q.Seq Literal

data UPData s = UPData {
                mapping :: InClauses,
                formula :: MVecFormula s,
                assigned :: MTrueClauses s
              }

type UPMonad s = ReaderT (UPData s) (StateT Queue (ST s))

evalUp :: UPMonad s a -> UPData s -> Queue -> ST s a
evalUp mon upData = evalStateT (runReaderT mon upData)

toVecFormula :: CNF -> VecFormula
toVecFormula = V.fromList . map S.fromList

-- | Dla każdego literału z formuły tworzy mapowanie na listę klauzul
-- w których ten literał występuje. Zarówno dla literału oraz jego negacji
-- musi istnieć przynajmniej pusta lista klauzul
buildMapping :: CNF -> InClauses
buildMapping cnf = foldl build initial (zip cnf [0, 1..])
    where build mapping (clause, ind) = foldl (addToMap ind) mapping clause
          addToMap ind mapping lit = case M.lookup lit mapping of
                                   Nothing    -> M.insert lit [ind] mapping
                                   Just list  -> M.insert lit (ind:list) mapping
          -- Tworzy początkowe mapowanie - każda zmienna musi być obecna
          initial = foldl addEmpty M.empty $ S.toList (variablesCNF cnf)
          addEmpty mapping var = M.union mapping (M.fromList [(Lit var, []), (Lit (-var), [])])

getInitState :: CNF -> ST s (UPData s)
getInitState cnf = do
    let literalMap = buildMapping cnf
    vecCNF  <- V.unsafeThaw (toVecFormula cnf)
    assigned <- UM.replicate (VM.length vecCNF) False
    return $ UPData literalMap vecCNF assigned

initLiteralQueue :: CNF -> Queue
initLiteralQueue = foldl add Q.empty
    where add que [lit] = que |> lit
          add que _ = que

-- | funkcja dla klauzul CNF zwraca uproszczone klauzule
-- po wykonaniu unit propagation oraz literały, do których przypisano
-- wartość True
-- Jeśli propagacja się nie udaje CNF będzie zawierał klauzulę pustą
unitPropagation :: CNF -> (CNF, [Literal])
unitPropagation cnf = runST $ do
  upData <- getInitState cnf

  trueLiterals <- evalUp (propagationLoop []) upData (initLiteralQueue cnf)

  newCnf <- V.unsafeFreeze (formula upData)
  trueClauses <- U.unsafeFreeze (assigned upData)
  let addClause clauses ind = S.toList (newCnf V.! ind) : clauses
      newFormula = U.foldl' addClause [] (U.elemIndices False trueClauses)

  return (newFormula, trueLiterals)
      where
        propagationLoop :: [Literal] -> (forall s. UPMonad s [Literal])
        propagationLoop trueLiterals = do
          emptyQueue <- gets Q.null
          if emptyQueue then return trueLiterals
          else do
            lit <- gets (`Q.index` 0)
            modify (Q.drop 1)

            vecCNF <- asks formula
            trueClauses <- asks assigned

            allPosClauses <- asks $ flip (M.!) lit . mapping
            allNegClauses <- asks $ flip (M.!) (neg lit) . mapping
            posClauses <- filterM (\ind -> not <$> UM.unsafeRead trueClauses ind) allPosClauses
            negClauses <- filterM (\ind -> not <$> UM.unsafeRead trueClauses ind) allNegClauses

            -- ustawienie na True klauzul w których występuje lit
            forM_ posClauses $ \cInd -> do
              -- lit mógł już zostać usunięty z klauzuli
              notEmptyClause <- not . S.null <$> VM.unsafeRead vecCNF cInd
              when notEmptyClause $
                UM.unsafeWrite trueClauses cInd True

            -- zaktualizowanie klauzul w których występuje ~lit
            forM_ negClauses $ \cInd -> do
              litSet <- VM.unsafeRead vecCNF cInd
              let newLitSet = S.delete (neg lit) litSet
              when (S.size newLitSet == 1) $
                modify (|> S.elemAt 0 newLitSet)
              VM.unsafeWrite vecCNF cInd newLitSet

            propagationLoop $ lit:trueLiterals
