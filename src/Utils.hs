module Utils where

import Monad
import CNF
import UP
import Types
import Data.Maybe
import Control.Monad.State
import Data.Vector.Unboxed ((!))
import qualified Data.Vector.Unboxed.Mutable as UV
import qualified Data.Vector.Unboxed as U

-- | Funkcja wczytuje cnfa z wejścia
readCNF :: IO CNF
readCNF = do
  contents <- filter (not . null) . map words . lines <$> getContents
  let input = filter (\line -> head line `notElem` ["c", "p", "%"]) contents
      integers = filter (not . null) . map (filter (/= 0) . map read) $ input :: [[Int]]
  return (fromIntsCNF integers)

-- | Zwraca Just cnf jeśli uproszczenie się udaje - np. gdy propagacja
-- unitów się nie wysypuje
simplifyCNF :: CNF -> Maybe (CNF, [Literal])
simplifyCNF cnf = let (cnf', literals) = unitPropagation . removeTrivials . removeDups . removeEmpty $ cnf
                  in if hasEmptyClause cnf' then Nothing
                                            else Just (cnf', literals)

-- | Zwraca listę literałów z ostatniego poziomu decyzyjnego
currentLevelLiterals :: FrozenVarLevels -> Int -> [Literal] -> [Literal]
currentLevelLiterals levels currentLevel =
    takeWhile (\lit -> levels ! var lit == currentLevel)

currentLevelLiteralsM :: FrozenVarLevels -> SolverM c s [Literal]
currentLevelLiteralsM levels = do
    currentLevel <- gets decLevel
    tr <- gets trail
    return $ currentLevelLiterals levels currentLevel tr

-- | Zwraca liczbę zmiennych występujących w problemie CNF
variablesCountM :: SolverM c s Int
variablesCountM = pred . UV.length <$> gets values

variablesCount :: FrozenBindings -> Int
variablesCount =  pred . U.length

-- | Zwraca literały bez przypisanej wartości
unassignedVars :: FrozenBindings -> U.Vector Int
unassignedVars = U.findIndices (0 ==)

assignedCount :: FrozenBindings -> Int
assignedCount = U.foldl' (\val acc -> acc + if val == 0 then 0 else 1) 0
