{-# LANGUAGE TemplateHaskell #-}
import CNF

import Test.QuickCheck
import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen
import Test.QuickCheck.All
import Test.CNF
import Data.Foldable
import Data.Maybe
import Data.Either
import qualified Data.Set as S
import qualified Data.IntMap as IM
import qualified Data.Vector.Unboxed as U

-- | Sprawdza czy check bindings zwraca Right na pełnych bindingach
prop_cnfAllBound = forAll (cnfGen 20 30) $ \cnf ->
    let
      count = maximum (S.toList (variablesCNF cnf))
      bindings = U.fromList . take (count + 1) . map (toBind . Just) $ trueFalseIntersparse
    in isRight $ checkBindings cnf bindings

-- | Sprawdza czy check bindings zwraca Left na niepełnych bindingach
prop_cnfNotAllBound = forAll solverGen $ \(cnf, bindings) ->
    isLeft $ checkBindings cnf bindings
      where solverGen = do
                frac <- choose (0.0, 0.4)
                cnf <- cnfGen 20 30
                bindings <- bindingsGen cnf frac
                return (cnf, bindings)

-- | uruchamia wszystkie testy
return []
main :: IO Bool
main = $quickCheckAll
