{-# LANGUAGE TemplateHaskell #-}
import CNFTests
import UPTests


return []
main :: IO Bool
main = do
    let cnf = fromIntsCNF [[1, 2, -3], [-2, 3], [1]]
    putStrLn $ show cnf
    return True
