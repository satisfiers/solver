{-# LANGUAGE TemplateHaskell #-}
import CNF
import UP

import Test.QuickCheck
import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen
import Test.QuickCheck.All
import Test.CNF
import Data.Foldable
import Data.Maybe
import Data.Either
import qualified Data.Set as S
import qualified Data.IntMap as IM
import qualified Data.Vector.Unboxed as U


return []
main :: IO Bool
main = do
    let cnf0 = fromIntsCNF [[1, 2, -3], [-2, 3], [4]]
        cnf1 = fromIntsCNF [[1, 2, -3], [-2, 3], [1]]
        cnf2 = fromIntsCNF [[1, 2, -3], [-2, 3], [2]]
        cnf3 = fromIntsCNF [[1], [2], [-3]]
        cnf4 = fromIntsCNF [[1, 2], [-1], [-2]]
        cnf5 = fromIntsCNF [[1, 2, 3], [3, 2], [-1], [-2]]
        cnf6 = fromIntsCNF [[1], [1, 2, -3], [-1, 3, 4], [-1,-2], [-1, -2, 3], [3, -4, 5], [-1, 2, 5]]
    print cnf0
    print (unitPropagation cnf0)
    putStrLn ""
    print cnf1
    print (unitPropagation cnf1)
    putStrLn ""
    print cnf2
    print (unitPropagation cnf2)
    putStrLn ""
    print cnf3
    print (unitPropagation cnf3)
    putStrLn ""
    print cnf4
    print (unitPropagation cnf4)
    putStrLn ""
    print cnf5
    print (unitPropagation cnf5)
    putStrLn ""
    print cnf6
    print (unitPropagation cnf6)
    return True
